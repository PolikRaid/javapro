package jpa1;

import com.sun.org.apache.bcel.internal.generic.ACONST_NULL;
import javafx.collections.transformation.TransformationList;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class App {
    static EntityManagerFactory emf;
    static EntityManager em;
    static List newAccounts = new ArrayList<Account>();
    static List newTransactions = new ArrayList<Transaction>();


    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        try {
            // create connection
            emf = Persistence.createEntityManagerFactory("JPABank");
            em = emf.createEntityManager();

            addExchangeRate("USD", "USD", 1.0);
            addExchangeRate("EUR", "EUR", 1.0);
            addExchangeRate("UAH", "UAH", 1.0);
            addExchangeRate("USD", "UAH", 26.5);
            addExchangeRate("EUR", "UAH", 29.5);
            addExchangeRate("UAH", "EUR", 0.0338);
            addExchangeRate("UAH", "USD", 0.0377);
            addExchangeRate("EUR", "USD", 1.11);
            addExchangeRate("USD", "EUR", 0.898);

            try {
                while (true) {
                    System.out.println("1: add client");
                    System.out.println("2: add account for client");
                    System.out.println("3: refill account");
                    System.out.println("4: transfer from account to account");
                    System.out.println("5: view client`s accounts");
                    System.out.print("-> ");

                    String s = sc.nextLine();
                    switch (s) {
                        case "1":
                            addClient(sc);
                            break;
                        case "2":
                            addAccount(sc);
                            break;
                        case "3":
                            refillAccount(sc);
                            break;
                        case "4":
                            transferToAccount(sc);
                            break;
                        case "5":
                            viewClientAccountsAndGetTotalBalance(sc);
                            break;
                        default:
                            return;
                    }
                }
            } finally {
                sc.close();
                em.close();
                emf.close();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return;
        }
    }

    private static void addExchangeRate(String currencyFrom, String currencyTo, Double exchangeRate) {
        em.getTransaction().begin();
        try {
            ExchangeRate er = new ExchangeRate(currencyFrom, currencyTo, exchangeRate);
            em.persist(er);
            em.getTransaction().commit();
        } catch (Exception ex) {
            em.getTransaction().rollback();
        }
    }

    private static void addClient(Scanner sc) {
        System.out.print("Enter client name: ");
        String name = sc.nextLine();
        System.out.print("Enter client age: ");
        String sAge = sc.nextLine();
        int age = Integer.parseInt(sAge);
        em.getTransaction().begin();
        try {
            Client c = new Client(name, age, newAccounts);
            em.persist(c);
            em.getTransaction().commit();

            System.out.println(c.getId());
        } catch (Exception ex) {
            em.getTransaction().rollback();
        }
    }


    private static Client findClient(String name) {
        Client c = null;
        try {
            Query query = em.createQuery(
                    "SELECT c FROM Client c WHERE c.name = :name", Client.class);
            query.setParameter("name", name);
            c = (Client) query.getSingleResult();
            return c;
        } catch (NoResultException ex) {
            System.out.println("Client not found!");
        } catch (NonUniqueResultException ex) {
            System.out.println("Non unique result!");
        }
        return null;
    }

    private static Account findAccountById(Long id) {
        Account a = null;
        try {
            Query query = em.createQuery(
                    "SELECT a FROM Account a WHERE a.id = :id", Account.class);
            query.setParameter("id", id);
            a = (Account) query.getSingleResult();
            return a;
        } catch (NoResultException ex) {
            System.out.println("Account not found!");
        } catch (NonUniqueResultException ex) {
            System.out.println("Non unique result!");
        }
        return null;
    }

    private static void addAccount(Scanner sc) {
        System.out.print("Enter client name: ");
        String name = sc.nextLine();
        System.out.print("Enter new currency (USD/EUR/UAH): ");
        String currency = sc.nextLine();
        System.out.print("Enter initial balance: ");
        String scbalance = sc.nextLine();
        Double balance = Double.parseDouble(scbalance);

        Client c = findClient(name);
        em.getTransaction().begin();
        try {
            Account a = new Account(balance, currency.toUpperCase(), c, newTransactions);
            em.persist(a);
            em.getTransaction().commit();
            System.out.println(a.toString());
        } catch (Exception ex) {
            em.getTransaction().rollback();
        }
    }

    private static void viewClientAccountsAndGetTotalBalance(Scanner sc) {
        System.out.print("Enter client name: ");
        String name = sc.nextLine();

        Client c = findClient(name);
        if (c==null){
            System.out.println("No client found");
            return;
        }
        Query query = em.createQuery(
                "SELECT a FROM Account a WHERE a.client = :c", Account.class);
        query.setParameter("c", c);
        List<Account> list = (List<Account>) query
                .getResultList();
        Double totalBalance = 0.0;
        for (Account a : list) {
            totalBalance = totalBalance + a.getBalance();
            System.out.println(a);
        }
        System.out.println("Total accounts balance: "+ totalBalance);
    }

    private static void refillAccount(Scanner sc) {
        System.out.print("Enter account id: ");
        String accId = sc.nextLine();
        Long id = new Long(accId);
        System.out.print("Enter amount ");
        String amount = sc.nextLine();
        Double amountDouble = new Double(amount);
        Account a = null;

        try{
            Query query = em.createQuery(
                "SELECT a FROM Account a WHERE a.id = :id", Account.class);
            query.setParameter("id", id);
            a = (Account) query.getSingleResult();
        } catch (NoResultException ex) {
            System.out.println("Account not found!");
        } catch (NonUniqueResultException ex) {
            System.out.println("Non unique result!");
        }

        em.getTransaction().begin();
        try {
            System.out.println("Start refilling account");
            a.setBalance(a.getBalance()+amountDouble);
            em.persist(a);
            em.getTransaction().commit();
            System.out.println("Refilled by amount "+amount+", new balance is: "+a.getBalance());

        } catch (Exception ex) {
            em.getTransaction().rollback();
        }

    }

    private static void transferToAccount(Scanner sc) {
            System.out.print("Enter sender account id: ");
            String SenderAccId1 = sc.nextLine();
            Long SenderAccId = new Long(SenderAccId1);
            System.out.print("Enter receiver account id: ");
            String ReceiverAccId1 = sc.nextLine();
            Long ReceiverAccId = new Long(ReceiverAccId1);
            System.out.print("Enter amount: ");
            String amount = sc.nextLine();
            Double amountDouble = new Double(amount);
            Account sender = findAccountById(SenderAccId);
        System.out.println("sender"+sender.toString());
            Account receiver = findAccountById(ReceiverAccId);
        System.out.println("receiver: "+receiver.toString());
            if (sender==null || receiver==null){
                System.out.println("No account found");
                return;
            }
        System.out.println("1");
            em.getTransaction().begin();
            try {
                System.out.println("2");
                sender.setBalance(sender.getBalance()-amountDouble);
                System.out.println("3");
                Double amountForReceiver = getExchangedAmount(sender.getCurrency(), receiver.getCurrency(), amountDouble);
                receiver.setBalance(receiver.getBalance()+amountForReceiver);
                em.persist(sender);
                em.persist(receiver);
                em.getTransaction().commit();
                System.out.println("transferred amount "+amount+", sender new balance is : "+ sender.getBalance() +"receiver new balance is: "+receiver.getBalance());

            } catch (Exception ex) {
                System.out.println("Transfer failed:"+ex);
                em.getTransaction().rollback();
            }

        }

    private static Double getExchangedAmount(String currencyFrom, String currencyTo, Double amount) {
        Double exchangedAmount = null;
        ExchangeRate er = null;
        try {
            Query query = em.createQuery(
                    "SELECT er FROM ExchangeRate er WHERE er.currencyFrom = :currencyFrom AND er.currencyTo = :currencyTo", ExchangeRate.class);
            query.setParameter("currencyFrom", currencyFrom);
            query.setParameter("currencyTo", currencyTo);
            er = (ExchangeRate) query.getSingleResult();
            exchangedAmount = er.getExchangeRate()*amount;
        } catch (NoResultException ex) {
            System.out.println("Account not found!");
        } catch (NonUniqueResultException ex) {
            System.out.println("Non unique result!");
        }

        return exchangedAmount;
    }

    static final String[] NAMES = {"Ivan", "Petr", "Andrey", "Vsevolod", "Dmitriy"};
    static final Random RND = new Random();

    static String randomName() {
        return NAMES[RND.nextInt(NAMES.length)];
    }
}


