import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.util.*;

@WebServlet("/upload")
public class MakeZipServlet extends HttpServlet {

    final static String template = "<html>\n" +
            "<head>" +
            "    <title>You can download archive on this page</title>" +
            "</head>" +
            "<body>%s</body></html>";

    private String saveUploadedFile(FileItem item, String uploadFolder) throws Exception {
        String path = "";
        if (item.getSize() > 0) {
            path = uploadFolder+File.separator+item.getName();
            item.write(new File(path));
        }
        return path;
    }

    private String makeDirectory(String dirName) {
        String rootDir = getServletContext().getRealPath(".");
        File makeDir = new File(rootDir+File.separator+dirName);
        makeDir.mkdir();
        return makeDir.getAbsolutePath();
    }

    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        final Map<String, String> mapFiles = new HashMap<>();

        String filesDir = makeDirectory("filesfolder");
        String archiveDir = makeDirectory("download");
        String archiveName = req.getParameter("archiveName");

        boolean isMultipart = ServletFileUpload.isMultipartContent(req);
        if (!isMultipart) {
            resp.sendError(HttpServletResponse.SC_BAD_REQUEST);
            return;
        }
        DiskFileItemFactory factory = new DiskFileItemFactory();
        factory.setSizeThreshold(1024 * 1024);
        factory.setRepository(new File(filesDir));
        ServletFileUpload upload = new ServletFileUpload(factory);
        upload.setSizeMax(1024 * 1024 * 10);
        try {
            List items = upload.parseRequest(req);
            Iterator iter = items.iterator();
            while (iter.hasNext()) {
                FileItem item = (FileItem) iter.next();
                System.out.println("item name: "+item.getName()+", string: , size"+item.getSize());
                if (item.isFormField()) {
                    if (item.getString().equals("")){
                    } else {
                        archiveName = item.getString();
                    }
                } else {
                    if (item.getSize() > 0) {
                    String uploadedFileName = saveUploadedFile(item, filesDir);
                    mapFiles.put(uploadedFileName, item.getName());
                        System.out.println("map size; "+mapFiles.size());
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            resp.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            return;
        }
        File zip = FIleManager.zipAttachments(mapFiles, archiveName, archiveDir);
        FileItem fi1 = factory.createItem(zip.getAbsolutePath(), "application/zip", false,
                zip.getName());
        try {
            fi1.write(zip);
        } catch (Exception e) {
            e.printStackTrace();
        }
        String msg =
        "<h1>Results page: </h1><a href=\"/download/"+ archiveName+".zip" + "\">Donwload Archive</a>\n"+
        "<a href=\"/index.jsp\">Back to file selection</a>\n";
        resp.getWriter().println(String.format(template, msg));
    }
}
