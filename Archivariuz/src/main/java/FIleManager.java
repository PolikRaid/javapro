import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.FileSystem;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.util.Comparator;
import java.util.Map;
import java.util.Set;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;
import static org.junit.Assert.*;

public class FIleManager {

    public static void deleteDirectoryWithFiles(String directoryPath) throws IOException {
        FileSystem fileSystem = FileSystems.getDefault();
        Path pathToBeDeleted = fileSystem.getPath(directoryPath);
        System.out.println("start FileManager.deleteDirectoryWithFiles to delete directory "+directoryPath);
        java.nio.file.Files.walk(pathToBeDeleted)
                .sorted(Comparator.reverseOrder())
                .map(Path::toFile)
                .forEach(File::delete);

        assertFalse("Directory still exists",
                java.nio.file.Files.exists(pathToBeDeleted));
    }

    public static File zipAttachments(Map<String, String> files, String name, String location) throws IOException
    {
        Set<Map.Entry<String, String>> filesSet = files.entrySet();
        byte[] buf = new byte[1024];
        File zipFile = new File(location+File.separator+name+".zip");
        FileOutputStream fos = new FileOutputStream(zipFile);
        ZipOutputStream zip = new ZipOutputStream(fos);
        for(Map.Entry<String, String> entry:filesSet)
        {
            ZipEntry zipEntry = new ZipEntry(entry.getValue());
            System.out.println("zip getValue11: "+entry.getValue());
            FileInputStream in = new FileInputStream(entry.getKey());
            System.out.println("zip getKey11: "+entry.getKey());

            zip.putNextEntry(zipEntry);
            int len;
            while ((len = in.read(buf)) > 0)
            {
                zip.write(buf, 0, len);
            }
            zip.closeEntry();
            in.close();
        }
        zip.close();

        return zipFile;
    }
}
