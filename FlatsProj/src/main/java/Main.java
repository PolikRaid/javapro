import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import java.util.Scanner;

public class Main {

    static final String DB_CONNECTION = "jdbc:mysql://localhost:3306/flat?serverTimezone=Europe/Kiev";
    static final String DB_USER = "root";
    static final String DB_PASSWORD = "pdmuser";

    public static void main(String[] args) throws SQLException {
        Scanner sc = new Scanner(System.in);

        ConnectionFactory factory = new ConnectionFactory(DB_CONNECTION,DB_USER,DB_PASSWORD);
        Connection conn = factory.getConnection();
        try {
            FlatDAO dao = new FlatDAO(conn, "flats");
            dao.init();

            while (true) {
                System.out.println("1: add flat");
                System.out.println("2: view flats");
                System.out.println("3: view all");
                System.out.println("4: filter flats");
                System.out.print("-> ");

                String s = sc.nextLine();
                switch (s) {
                    case "1":
                        System.out.print("Enter district: ");
                         String location = sc.nextLine();
                         System.out.print("Enter address: ");
                         String address = sc.nextLine();
                         System.out.print("Enter area: ");
                         int area = sc.nextInt();
                         System.out.print("Enter number of rooms: ");
                         int roomsnumber = sc.nextInt();
                         System.out.print("Enter price: ");
                         int price = sc.nextInt();

                        dao.add(new Flat(location, address, area, roomsnumber, price));
                        break;
                    case "2":
                        List<Flat> list = dao.getAll(Flat.class);
                        for (Flat flat : list) {
                            System.out.println(flat);
                        }
                        break;
                    case "3":
                        System.out.println(dao.getAll(Flat.class));
                        break;
                    case "4":
                        System.out.print("To get filtered records ");
                        System.out.print("Enter param (area, roomsnumber, price):");
                        String param = sc.nextLine();
                        System.out.print("Enter (more, less, equals): ");
                        String operation = sc.nextLine();
                        String criteria = "";
                        switch (operation) {
                            case "more":
                                criteria = ">=";
                                break;
                            case "equals":
                                criteria = "=";
                                break;
                            case "less":
                                criteria = "<=";
                                break;
                            default:
                                return;
                        }
                        System.out.print("Enter (more, less, equals): ");
                        int value = sc.nextInt();
                        System.out.println(dao.getWhere(Flat.class, param, criteria, value));
                        break;
                    //default:
                        //return;
                }
            }
        } finally {
            sc.close();
            if (conn != null) conn.close();
        }
    }
}
