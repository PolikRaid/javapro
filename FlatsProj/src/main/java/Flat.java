
public class Flat {
    @Id
    private Integer id;
    private String location;
    private String address;
    private Integer area;
    private Integer roomsnumber;
    private Integer price;

    public Flat() {
    }

    public Flat(String location, String address, Integer area, Integer roomsnumber, Integer price) {

        this.location = location;
        this.address = address;
        this.area = area;
        this.roomsnumber = roomsnumber;
        this.price = price;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Integer getArea() {
        return area;
    }

    public void setArea(Integer area) {
        this.area = area;
    }

    public Integer getRoomsnumber() {
        return roomsnumber;
    }

    public void setRoomsnumber(Integer roomsnumber) {
        this.roomsnumber = roomsnumber;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "Flat{" +
                "id=" + id +
                ", location='" + location + '\'' +
                ", address='" + address + '\'' +
                ", area=" + area +
                ", roomsnumber=" + roomsnumber +
                ", price=" + price +
                '}';
    }
}
