import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

public class FlatDAO extends AbstractDAO<Integer, Flat>{

    public FlatDAO(Connection conn, String table) {
        super(conn, table);
    }

    @Override
    public void init() {
        try {
            try (Statement st = conn.createStatement()) {
                st.execute("DROP TABLE IF EXISTS Flats");
                st.execute("CREATE TABLE Flats (id INT NOT NULL AUTO_INCREMENT PRIMARY KEY, location VARCHAR(20), address VARCHAR(20), area INT NOT NULL, roomsnumber INT NOT NULL, price INT NOT NULL)");
            }
        } catch (SQLException ex) {
            throw new RuntimeException(ex);
        }
    }
}
