package ua.kiev.prog;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.List;

public class GetSurveyResultsServlet extends HttpServlet {

    private AnswersList answersList = AnswersList.getInstance();

    final static String TEMPLATE = "<!DOCTYPE html>" +
            "<html><head>" +
            "<style>" +
            "td, th {border: 1px solid #dddddd"+";"+" text-align: left; padding: 8px"+";"+"}" +
            "tr:nth-child(even) {background-color: #dddddd"+";"+"}" +
            "</style>" +
            "</head><body><h2>Survey results</h2>" +
            "<table><tr>" +
            "<th># of Users</th>" +
            "<th>Males number</th>" +
            "<th>Females number</th>" +
            "<th># of drivers</th>" +
            "<th># who does not drive</th>%s</table>" +
            "<a href=\"/index.jsp\">Back to questions</a>"+
            "</body></html>";

    public void doGet (HttpServletRequest req, HttpServletResponse resp) throws IOException {
        Integer usersNum = 0;
        Integer yesCount = 0;
        Integer noCount = 0;
        Integer maleCount = 0;
        Integer femaleCount = 0;

        List<Answer> list = answersList.GetJsonAnswers();
        for (Answer answers:list) {
            usersNum++;
            if(answers.getFirstAnswer().toLowerCase().equals("male")){
                maleCount++;}
            else {
                femaleCount++;
            }
            if(answers.getSecondAnswer().toLowerCase().equals("yes")) {
                yesCount++;
            } else {
                noCount++;
            }
        }

        String results = "<tr>" +
                "<td>"+usersNum+"</td>" +
                "<td>"+maleCount+"</td>" +
                "<td>"+femaleCount+"</td>" +
                "<td>"+yesCount+"</td>" +
                "<td>"+noCount+"</td>" +
                "</tr>";

        resp.getWriter().println(String.format(TEMPLATE, results));
    }
}
