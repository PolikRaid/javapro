package ua.kiev.prog;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class DoSurveyServlet extends HttpServlet {

    private AnswersList answerList = AnswersList.getInstance();

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        String firstName = req.getParameter("firstName");
        String lastName = req.getParameter("lastName");
        String firstAnswer = req.getParameter("gender");
        String secondAnswer = req.getParameter("car");
        Answer answer = new Answer();
        if (firstName != null && lastName != null && firstAnswer != null &&  secondAnswer != null){
            answer.setFirstName(firstName);
            answer.setLastName(lastName);
            answer.setFirstAnswer(firstAnswer);
            answer.setSecondAnswer(secondAnswer);
            answerList.add(answer);

            resp.sendRedirect("/survey");
        }
        else {
            resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
        }
    }
}
