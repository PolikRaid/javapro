package ua.kiev.prog;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class AnswersList {
    private static final AnswersList answerList = new AnswersList();

    private final Gson gson;
    private final List<Answer> list = new LinkedList<>();

    public AnswersList() {
        gson = new GsonBuilder().create();
    }

    public static AnswersList getInstance() {
        return answerList;
    }

    public synchronized void add(Answer m) {
        list.add(m);
    }

    public List<Answer> GetJsonAnswers() {
        List<Answer> answerslist = new ArrayList<>();
        for (int i = 0; i < list.size(); i++)
            answerslist.add(list.get(i));
        return answerslist;
    }
}

