package ua.kiev.prog;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class Answer {
    private String firstName;
    private String lastName;
    private String firstAnswer;
    private String secondAnswer;

    public String toJSON() {
        Gson gson = new GsonBuilder().create();
        return gson.toJson(this);
    }

    public static Answer fromJSON(String s) {
        Gson gson = new GsonBuilder().create();
        return gson.fromJson(s, Answer.class);
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstAnswer() {
        return firstAnswer;
    }

    public void setFirstAnswer(String firstAnswer) {
        this.firstAnswer = firstAnswer;
    }

    public String getSecondAnswer() {
        return secondAnswer;
    }

    public void setSecondAnswer(String secondAnswer) {
        this.secondAnswer = secondAnswer;
    }

    @Override
    public String toString() {
        return new StringBuilder().append("[First Name: ").append(firstName)
                .append(", Last Name: ").append(lastName).append(", First answer: ").append(firstAnswer)
                .append(", Second answer: ").append(secondAnswer).append("] ")
                .toString();
    }
}
